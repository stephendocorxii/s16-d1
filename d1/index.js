/*Loops*/

/*Instruction: Display "JUan Dela Cruz" on our console 10x*/

console.log("Juan Dela Cruz");

/*Instruction: Display each element available on our array*/

let students =["TJ","Mia","Tin","Chris"];

console.log(students[0]);
console.log(students[1]);
console.log(students[2]);
console.log(students[3]);

/*While Loop*/

let count = 5;/*number of iteration, number of times of how many we repeat our code*/

//while(/*condition*/){ //condition - evaluates a given code if it is true or false - if the condition is true, the loop will start and continue our iteration or the repetition of our block of code
	//block of code - this will be repeated by the loop
	// counter for our iteration - this is the reason of continous loop/iteration
//}

/*example
Instructions: Repeat a name "Sylvan" 5x
*/

/*while(count !== 0) {
	console.log("Sylvan");
	count--; //will be decremented by 1
}*/

/*Instruction: Print numbers 1 to 5 using while*/

/*let number = 1;

while (number <= 5) {
	console.log(number);
	number ++;
}
*/

/*let fruits = ['Banana', 'Mango'];

let indexNumber = 0;

while(indexNumber <= 1){ 
//the condition is based on the last index of the elements that we have on an array
	console.log(fruits[indexNumber]); 
	//will get the elements on the inside of the array based on the indexNumber value
	indexNumber++;
}
*/

/*let mobilePhones = ['Samsung Galaxy s21', 'Iphone 13 Pro', 'Xiaomi 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus ROG 6'];

console.log(mobilePhones.length);
console.log(mobilePhones.length - 1);
console.log(mobilePhones[mobilePhones.length - 1]);

let indexNumberForMobile = 0;

while(indexNumberForMobile <= mobilePhones.length-1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}*/

/*Do-While - do the statement once, before going to the condition*/

/*let countA = 1;

do {
	console.log("Juan");
	countA++; 
} while(countA <= 6);*/
console.log("===========Do-While VS While===================")


let countB = 6;

do{
	console.log(`Do-While count ${countB}`);
	countB--;
} while(countB == 7);


while(countB == 7) {
	console.log(`While Count ${countB}`);
	countB--;
}

/*
Mini activity
	Instructions: with a given array, kindly display each elements on the console using do-while loop
*/

let indexNumberA = 0;

let computerBrands = ['Apple Macbook Pro', 'HP Notebook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];

do {
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
} while (indexNumberA <= computerBrands.length -1);

/*For Loop*/

console.log("===============For Loop==========================");

/*for(let count=5; count <= 0; count--){
	console.log(count);
}*/

let colors = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'White', 'Black'];

for(let count=0; count <=6; count++){
	console.log(colors[count]);
}

/*Continue & Break
	Break - stops the execution of our code
	Continue - skip a block of code and continue to the next iteration
*/

/*
ages
	18,19,20,21,24,25
	age == 21 (debutante age of boys), we will skip then go to the next iteration
	18,19,20,24,25
*/

let ages = [18,19,20,21,24,25];
/*skip the debutante of boys and girls using keyword*/
for(let i = 0; i <=ages.length - 1; i++){
	if(ages[i] == 21 || ages[i] == 18){
		continue;
	}
	console.log(ages[i]);
}

/*
	let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];

	Once we found Jayson on our array, we will stop the loop

	Den
	Jayson
*/

let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
for (let i = 0; i <= studentNames.length - 1; i++) {
	if(studentNames[i] == 'Jayson'){
	console.log(studentNames[i])
	break;
	}
}

